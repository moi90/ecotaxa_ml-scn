/*
 * ClassList.cpp
 *
 *  Created on: 15.09.2017
 *      Author: mschroeder
 */

#include "ClassList.h"

#include <fstream>
#include <iostream>
#include <sstream>

ClassList::ClassList(std::string classesListFile) {
	std::ifstream f(classesListFile.c_str());

	if(!f) {
		std::stringstream error;
		error << "Error: Classes file does not exist: " << classesListFile;
		throw std::runtime_error(error.str());
	}

	std::string cl;
	int ctr = 0;
	while (f >> cl) {
		classes[cl] = ctr++;
	}
}

int ClassList::nClasses() {
	return classes.size();
}

void ClassList::summary() {
	for (auto const& x : classes) {
		std::cout << "\"" << x.first << "\":" << x.second << std::endl;
	}
}

int ClassList::getNumeric(std::string label) {
	if(classes.count(label) > 0) {
		return classes.at(label);
	}
	return -1;
}
