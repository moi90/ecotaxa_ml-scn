#include "NetworkArchitectures.h"
#include "SpatiallySparseDatasetOpenCV.h"
#include "ClassList.h"
#include "OpenCVPicture.h"
#include "SpatiallySparseDatasetOpenCVIndex.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include <memory>

std::string getenv_default(const char *env_var,
		const char *default_val = NULL) {
	const char *result = std::getenv(env_var);
	if (result == NULL) {
		if (default_val == NULL) {
			throw std::invalid_argument(
					std::string("Env var not set: ") + env_var);
		}
		return std::string(default_val);
	}
	return std::string(result);
}

// -------------------------------------------------------------------------
// Network definitions
// -------------------------------------------------------------------------
// We will allow for three types of convolutional network architecture:
// 1) A simple but effective, not-very-deep-by modern-standards network inspired
// by "Multi-column Deep Neural Networks for Image Classification", Dan Ciresan,
// Ueli Meier and Jurgen Schmidhuber.
// C3-MP2-C2-MP2-...-MP2-C2-AveragePooling
// If there are nLayers of MaxPooling, the network is optimised to detect
// features with size 2^nLayers
// 2) A deeper network with VGG-style C3C3 paired convolutional layers: "Very
// Deep Convolutional Networks for Large-Scale Visual Recognition", Karen
// Simonyan and Andrew Zisserman
// C3-C3-MP2-C3-C3-MP2-...-MP2-C3-C3-AveragePooling
// If there are nLayers of MaxPooling, the network is optimised to detect
// features with size 2^nLayers
// 3) A deeper network with fractional max-pooling. "Fractional Max-Pooling",
// Ben Graham
// C2-FMP-...-C2-FMP-C2-AveragePooling
// If there are nLayers of MaxPooling, the network is optimised to detect
// features with size 2^(nLayers/2), i.e. nLayers needs to be larger

class PlanktonSparseConvNet: public SparseConvNet {
public:
	PlanktonSparseConvNet(int nInputFeatures, int nClasses, int networkType, // 1, 2 or 3, as above
			int nLayers,     // number of layers of max-pooling (i.e. 5 or 6 for
							 // network types 1 and 2, 10 or 12 for type 2)
			float dropoutMultiplier = 0, // number between 0 and 0.5. Dropout can reduce
										 // over-fitting when training data is limited.
										 // An increasing amount of dropout is used
										 // rising up through the network from 0 to
										 // dropoutMultiplier.
			int cudaDevice = -1 // PCI Bus ID for the CUDA device to use, -1 for the default device
			) :
			SparseConvNet(2, nInputFeatures, nClasses, cudaDevice) {

		switch (networkType) {

		case 1: // Ciresan, et al, simple net
			for (int i = 0; i < nLayers; i++) {
				// convolution + max pooling
				addLeNetLayerMP(32 * (i + 1), // number of filters in the i-th layer
				(i == 0) ? 3 : 2, // filter size, 3x3 convolution in
								  // layer 0, 2x2 convolution in higher
								  // layers
				1,                // filter stride (i.e. shift)
						2,                // max-pooling size
						2,                // max-pooling stride
						VLEAKYRELU,       // activation function
						dropoutMultiplier * i / (nLayers + 1) // dropout probability
								);
			}
			// convolution only (no pooling)
			addLeNetLayerMP(32 * (nLayers + 1), 2, 1, 1, 1, VLEAKYRELU,
					dropoutMultiplier * nLayers / (nLayers + 1));
			break;

		case 2: // VGG-style net
			for (int i = 0; i < nLayers; i++) {
				// convolution over 3x3 px, no pooling
				addLeNetLayerMP(32 * (i + 1), 3, 1, 1, 1, VLEAKYRELU,
						dropoutMultiplier * i / (nLayers + 1));
				// convolution over 3x3 and pooling over 2x2
				addLeNetLayerMP(32 * (i + 1), 3, 1, 2, 2, VLEAKYRELU,
						dropoutMultiplier * i / (nLayers + 1));
			}
			// convolutions only
			addLeNetLayerMP(32 * (nLayers + 1), 3, 1, 1, 1, VLEAKYRELU,
					dropoutMultiplier * nLayers / (nLayers + 1));
			addLeNetLayerMP(32 * (nLayers + 1), 3, 1, 1, 1, VLEAKYRELU,
					dropoutMultiplier * nLayers / (nLayers + 1));
			break;

		case 3: // Fractional max-pooling net
			// fractional max-pooling ratio
			const float fmpShrink = powf(2, 0.5);
			for (int i = 0; i < nLayers; i++) {
				// convolution + fractional max pooling
				addLeNetLayerPOFMP(32 * (i + 1), // number of filters in the i-th layer
				2,            // filter size, 2x2
						1,            // filter stride (i.e. shift)
						2,            // fractional max-pooling window size 2x2
						fmpShrink,    // fractional max-pooling stride
						VLEAKYRELU,   // activation function
						dropoutMultiplier * i / (nLayers + 1) // dropout probability
								);
			}
			// convolution only (no pooling)
			addLeNetLayerMP(32 * (nLayers + 1), 2, 1, 1, 1, VLEAKYRELU,
					dropoutMultiplier * nLayers / (nLayers + 1));
			break;
		}

		// final layer has spatial size 32x32, do average-pooling over active
		// sites
		addTerminalPoolingLayer(32);
		// fully connected layer
		addLeNetLayerMP(32 * (nLayers + 2), 1, 1, 1, 1, VLEAKYRELU,
				dropoutMultiplier);
		// softmax for classification
		addSoftmaxLayer();
	}
};

// -------------------------------------------------------------------------
// Data augmentation
// -------------------------------------------------------------------------
// Method to distort and scale input images
// used for data augmentation and to limit GPU memory usage
// Used in BatchProducer::preprocessBatch called by BatchProducer::nextBatch
// used in training, testing and validation.

//float areaThreshold; // global variable, set below, threshold for reducing image
// size
double maxDiagonalSize; // Maximum diagonal size of objects
int openCVflag;      // 0 for grayscale, >0 for color
int cropBottom = 0;

Picture *OpenCVPicture::distort(RNG &rng, batchType type) {
	// load image
	OpenCVPicture *pic = new OpenCVPicture(*this);
	pic->loadDataWithoutScaling(openCVflag);

	if (cropBottom > 0) {
		cv::Rect bbox(0, 0, pic->mat.cols, pic->mat.rows - cropBottom);
		cv::Mat tmp;
		(pic->mat)(bbox).copyTo(tmp);
		pic->mat = tmp;

		// Update offsets
		pic->xOffset = -tmp.cols / 2;
		pic->yOffset = -tmp.rows / 2;
	}

	cv::Size size = pic->mat.size();
	double diagonal = std::hypot(size.width, size.height);

	// int area = pic->area(); // number of active pixels
	float r;                // aspect ratio adjustment
	float s;                // scale
	float alpha;            // rotation
	float beta;             // shear

	// data augmentation
	if (type == TRAINBATCH) {
		// For training: rotate, shear and scale
		r = rng.uniform(-0.1, 0.1);
		s = 1 + rng.uniform(-0.1, 0.1);
		alpha = rng.uniform(0, 2 * 3.1415926535);
		beta = rng.uniform(-0.2, 0.2) + alpha;
	} else {
		// For testing: rotate only
		r = 0;
		s = 1;
		alpha = rng.uniform(0, 2 * 3.1415926535);
		beta = alpha;
	}

	/*// downscale large images while retaining most size information
	 if (area > areaThreshold) {
	 s *= powf(area / areaThreshold, -0.8);
	 }*/

	/*
	 Downscale images larger than 0.5 * maxDiagonalSize using the logistic function
	 to a maximum size of maxDiagonalSize.
	 */
	double linDiagonalSize = 0.5 * maxDiagonalSize;
	if (diagonal > linDiagonalSize) {
		double k = 4 / maxDiagonalSize;
		double newDiagonal = maxDiagonalSize
				/ (1 + std::exp(-k * (diagonal - linDiagonalSize)));
		s *= newDiagonal / diagonal;
	}

	// compute affine transformation matrix
	float c00 = (1 + r) * s * cos(alpha);
	float c01 = (1 + r) * s * sin(alpha);
	float c10 = -(1 - r) * s * sin(beta);
	float c11 = (1 - r) * s * cos(beta);

	// horizontal flip
	if (rng.randint(2) == 0) {
		c00 *= -1;
		c01 *= -1;
	}

	// distort image
	pic->affineTransform(c00, c01, c10, c11);

	// translate image within input field
	pic->jiggle(rng, 100);

	return pic;
}

//-------------------------------------------------------------------------
// main() function. Could be turned into a command line program or library
//-------------------------------------------------------------------------

int main() {

	//-------------------------------------------------------------------------
	// Options.
	//-------------------------------------------------------------------------

	// Network specification
	int networkType = 3;
	int nLayers = 12;
	int dropoutMultiplier = 0.0;

	// address of CUDA card (PCI bus id)
	// use nvidia-smi to get it; -1 for default GPU
	int cudaDevice = -1;

	// Start and stop training epochs
	// Starting at epoch > 0 loads the weights from that epoch and continues
	// training until stop epoch
	// If startEpoch==stopEpoch then just classify the unlabeledDataset
	// For training, stopEpoch should be roughly 4 / learningRateDecay
	int startEpoch = std::stoi(getenv_default("EPOCH", "99"));
	int stopEpoch = std::stoi(getenv_default("STOP_EPOCH", "0"));
	int exemplarsPerClassPerEpoch = 1000;
	float initialLearningRate = std::stof(
			getenv_default("LEARNING_RATE", "0.003"));
	float learningRateDecay = std::stof(getenv_default("DECAY", "0.05"));

	// number of images that are fed to the card at a time. Larger numbers will
	// (marginally) increase training speed. Increase until 2/3 of GPU memory are
	// used. If batchSize is small, make momentum closer to 1. Momentum increases
	// the effective batch size to batchSize * 1/(1 - momentum); that should cover
	// the diversity of the training set
	int batchSize = 40;
	float momentum = 0.999;

	// Threshold, in terms of active pixels, at which to start scaling down images
	//areaThreshold = 4000; // global variable

	// Maximum diagonal size of objects
	maxDiagonalSize = 300;

	// Path to training data file (labeled)
	std::string trainDataFn = getenv_default("TRAINING_DATA_FN", "");

	// Path to validation data file (labeled)
	std::string valDataFn = getenv_default("VALIDATION_DATA_FN", "");

	// Path to testing data file (labeled)
	std::string testingDataFn = getenv_default("TESTING_DATA_FN", "");

	// Path to prediction data file (unlabeled)
	std::string unlabeledDataFn = getenv_default("UNLABELED_DATA_FN", "");

	// OpenCV flag (1 for monochrome, 3 for RGB color images)
	int nFeatures = 1;
	openCVflag = (nFeatures == 1) ? 0 : 1; // global variable

	// Model root (where classlist and weights are stored)
	std::string modelDir = getenv_default("MODEL_DIR");

	// Model root (where classlist and weights are stored)
	std::string outputDir = getenv_default("OUTPUT_DIR");

	bool predict = std::stoi(getenv_default("PREDICT", "0"));
	bool dumpFeatures = std::stoi(getenv_default("DUMP_FEATURES", "1"));
	bool preloadFiles = std::stoi(getenv_default("PRELOAD_FILES", "0"));

	// Load the class list
	ClassList classList(modelDir + "/classes.txt");
	int nClasses = classList.nClasses();

	//-------------------------------------------------------------------------
	// Options summary
	//-------------------------------------------------------------------------

	switch (networkType) {
	case 1:
		std::cout << "Network type 1 - Ciresan-Schmidhuber-Meier style network"
				<< std::endl;
		break;
	case 2:
		std::cout << "Network type 2 - VGG style network" << std::endl;
		break;
	case 3:
		std::cout << "Network type 3 - fractional max-pooling network"
				<< std::endl;
		break;
	}
	std::cout << "Layers of pooling:         " << nLayers << std::endl;
	std::cout << "Dropout multiplier:        " << dropoutMultiplier
			<< std::endl;
	std::cout << "Start epoch:               " << startEpoch << std::endl;
	std::cout << "Stop epoch:                " << stopEpoch << std::endl;
	std::cout << "Exemplars/class/epoch:     " << exemplarsPerClassPerEpoch
			<< std::endl;
	std::cout << "Initial learning rate:     " << initialLearningRate
			<< std::endl;
	std::cout << "Learning rate decay:       " << learningRateDecay
			<< std::endl;
	std::cout << "Batch size:                " << batchSize << std::endl;
	std::cout << "Momentum:                  " << momentum << std::endl;
	//std::cout << "Area threshold:          " << areaThreshold << std::endl;
	std::cout << "Maximum diagonal:          " << maxDiagonalSize << std::endl;
	std::cout << "Training data file:        "
			<< (trainDataFn.empty() ? "(empty)" : trainDataFn) << std::endl;
	std::cout << "Validation data file:      "
			<< (valDataFn.empty() ? "(empty)" : valDataFn) << std::endl;
	std::cout << "Testing data file:         "
			<< (testingDataFn.empty() ? "(empty)" : testingDataFn) << std::endl;
	std::cout << "Unlabeled data file:       "
			<< (unlabeledDataFn.empty() ? "(empty)" : unlabeledDataFn)
			<< std::endl;
	std::cout << "Preload image files:       "
			<< (preloadFiles ? "true" : "false") << std::endl;
	std::cout << "Model directory:           " << modelDir << std::endl;
	std::cout << "Output directory:          " << outputDir << std::endl;
	std::cout << "nClasses                   " << nClasses << std::endl;
	std::cout << std::endl;

	//-------------------------------------------------------------------------
	// Training and using the network
	//-------------------------------------------------------------------------

	std::unique_ptr<SpatiallySparseDataset> trainSet;
	std::unique_ptr<SpatiallySparseDataset> validationSet;
	std::unique_ptr<SpatiallySparseDataset> testSet;
	std::unique_ptr<SpatiallySparseDataset> unlabeledSet;

	if (!trainDataFn.empty()) {
		// Load training data and/or count classes
		std::cout << "Loading training set..." << std::endl;
		trainSet.reset(
				new SpatiallySparseDatasetOpenCVIndex(classList, trainDataFn,
						TRAINBATCH, 255, preloadFiles, openCVflag));
		trainSet->summary();
		std::cout << std::endl;
	}

	if (!valDataFn.empty()) {
		std::cout << "Loading validation set ..." << std::endl;
		trainSet.reset(
				new SpatiallySparseDatasetOpenCVIndex(classList, valDataFn,
						TESTBATCH, 255, preloadFiles, openCVflag));
		validationSet->summary();
		std::cout << std::endl;
	}

	if (!testingDataFn.empty()) {
		std::cout << "Loading testing set..." << std::endl;
		testSet.reset(
				new SpatiallySparseDatasetOpenCVIndex(classList, testingDataFn,
						TESTBATCH, 255, preloadFiles, openCVflag));
		testSet->summary();
	}

	if (!unlabeledDataFn.empty()) {
		std::cout << "Loading unlabeled set..." << std::endl;
		unlabeledSet.reset(
				new SpatiallySparseDatasetOpenCVIndex(classList,
						unlabeledDataFn, UNLABELEDBATCH, 255,
						preloadFiles, openCVflag));
		unlabeledSet->summary();
	}

	// create network
	std::cout << "Create network:" << std::endl;
	PlanktonSparseConvNet cnn(nFeatures, nClasses, networkType, nLayers,
			dropoutMultiplier, cudaDevice);
	std::cout << std::endl;

	// load weights if continuing a previous run
	if (startEpoch > 0) {
		std::cout << "Loading weights..." << std::endl;
		cnn.loadWeights(modelDir + "/", startEpoch);
	}

	// training loop
	if (startEpoch < stopEpoch) {
		if (!trainSet) {
			throw std::runtime_error(
					"Error: startEpoch < stopEpoch, but no training set is provided.");
		}

		std::cout << "Training..." << std::endl;

		for (int epoch = startEpoch; epoch < stopEpoch; epoch++) {
			// Decrease learning rate with epoch
			// Start with large steps when the network is new.
			// If the initial learning rate is too big, the network will explode!
			// Decrease afterwards to fine tune the network
			float learningRate = initialLearningRate
					* exp(-learningRateDecay * epoch);

			std::cout << "epoch: " << epoch << " lr: " << learningRate
					<< std::endl;

			// One epoch consists of nClasses * exemplarsPerClassPerEpoch images
			auto trainSample = trainSet->balancedSample(
					exemplarsPerClassPerEpoch);

			// Train
			cnn.processDataset(trainSample,		// subset of images to train on
					batchSize,	// number of images to feed to the GPU at a time
					learningRate, momentum);

			// Save current weights
			cnn.saveWeights(modelDir + "/", epoch);

			if (validationSet) {
				// Perform validation on a representative sample of the validation set
				// with on average 100 images per class.
				auto validationSample = validationSet->subset(
						validationSet->nClasses * 100);
				//auto validationSample = validationSet->balancedSample(10);

				// Use multiple testing
				cnn.processDatasetRepeatTest(validationSample,// dataset to predict
						batchSize,// number of images to feed to the GPU at a time
						3			// number of repetitions of the prediction
						);
			}
		}
	}

	if (trainSet && dumpFeatures) {
		std::cout << "Dumping features of the training set..." << std::endl;
		cnn.processDatasetDumpTopLevelFeatures(*trainSet, batchSize, 1,
				outputDir + "/training_features.csv");
	}

	if (trainSet && predict) {
		std::cout << "Predicting full training set..." << std::endl;
		cnn.processDatasetRepeatTest(*trainSet,	// dataset to predict
				batchSize,		// number of images to feed to the GPU at a time
				3,				// number of repetitions of the prediction
				outputDir + "/training_predictions.csv",// file name for predictions
				outputDir + "/training_confusion.csv"// file name for confusion matrix
						);
	}

	if (validationSet && dumpFeatures) {
		std::cout << "Dumping features of the validation set..." << std::endl;
		cnn.processDatasetDumpTopLevelFeatures(*validationSet, batchSize, 1,
				outputDir + "/validation_features.csv");
	}

	if (validationSet && predict) {
		std::cout << "Predicting full validation set..." << std::endl;
		cnn.processDatasetRepeatTest(*validationSet,	// dataset to predict
				batchSize,		// number of images to feed to the GPU at a time
				3,				// number of repetitions of the prediction
				outputDir + "/validation_predictions.csv",// file name for predictions
				outputDir + "/validation_confusion.csv"	// file name for confusion matrix
						);
	}

	if (testSet && dumpFeatures) {
		std::cout << "Dumping features of the testing set..." << std::endl;
		cnn.processDatasetDumpTopLevelFeatures(*testSet, batchSize, 1,
				outputDir + "/testing_features.csv");
	}

	if (testSet && predict) {
		// predict testing set with repeat testing
		std::cout << "Predicting testing set..." << std::endl;
		cnn.processDatasetRepeatTest(*testSet, batchSize, 3,
				outputDir + "/testing_predictions.csv",
				outputDir + "/testing_confusion.csv");
	}

	if (unlabeledSet && dumpFeatures) {
		std::cout << "Dumping features of the unlabeled set..." << std::endl;
		// Dump top-level features for the validation set
		cnn.processDatasetDumpTopLevelFeatures(*unlabeledSet, batchSize, 1,
				outputDir + "/unlabeled_features.csv");
	}

	if (unlabeledSet && predict) {
		std::cout << "Predicting unlabeled set..." << std::endl;
		cnn.processDatasetRepeatTest(*unlabeledSet, batchSize, 3,
				outputDir + "/unlabeled_predictions.csv");
	}
}
