#include "SpatiallySparseDatasetOpenCVIndex.h"
#include "ClassList.h"

#include <iostream>
#include <cstdlib>
#include <string>

int openCVflag = 0;

Picture *OpenCVPicture::distort(RNG &rng, batchType type) {
	loadDataWithoutScaling(openCVflag);
	return this;
}

int main() {
	std::string classesListFile =
			"classes.txt";
	std::string indexFile =
			"/data-ssd/mschroeder/Ecotaxa/Datasets/flowcam_group1_collection_test_ecotaxa.csv";

	std::cout << "Loading class list..." << std::endl;
	ClassList classList(classesListFile);
	classList.summary();

	std::cout << "Loading training set..." << std::endl;
	SpatiallySparseDatasetOpenCVIndex trainSet(classList,
			indexFile,                // path to data
			UNLABELEDBATCH,                  // type of dataset
			255, // background grey level (tolerance +/- 2 set in
				 // OpenCVPicture.cpp)
			true, openCVflag);
	trainSet.summary();
}
