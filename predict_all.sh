#!/usr/bin/env bash
# Predict all results directories containing a .keep file

: "${RESULTS_ROOT:?RESULTS_ROOT needs to be nonempty}"
: "${DATASET_ROOT:?DATASET_ROOT needs to be nonempty}"
: "${CUDA_VISIBLE_DEVICES:?CUDA_VISIBLE_DEVICES needs to be nonempty}"

echo Using datasets ${DATASETS:=flowcam uvp5ccelter zoocam zooscan}
echo Using groupings ${GROUPINGS:=group1 group2}
echo Using splits ${SPLITS:=0 1 2}

for DATASET in $DATASETS; do
	export DATASET
	echo Dataset $DATASET
	for GROUPING in $GROUPINGS; do
		export GROUPING
		echo Grouping $GROUPING

		for SPLIT in $SPLITS; do
			export SPLIT
			echo Split $SPLIT

			export TRAINING_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_${SPLIT}_train/
			export VALIDATION_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_${SPLIT}_val/
			export CLASS_LIST_FN=${DATASET_ROOT}/${DATASET}_classes_${GROUPING}.txt
			export TEST_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_test
			export RESULTS_DIR=${RESULTS_ROOT}/SCN_${DATASET}_${GROUPING}_${SPLIT}/
			export START_EPOCH=99
			export STOP_EPOCH=99

			if [ ! -d $RESULTS_DIR ]; then
				echo $RESULTS_DIR is missing. Skipping.
				echo SCN_${DATASET}_${GROUPING}_${SPLIT} >> ${RESULTS_ROOT}/SCN_missing
				continue
			fi

			if [ ! -e $RESULTS_DIR/_epoch-99.cnn ]; then
				echo $RESULTS_DIR/_epoch-99.cnn is missing. Skipping.
				echo SCN_${DATASET}_${GROUPING}_${SPLIT} >> ${RESULTS_ROOT}/SCN_retrain
				continue
			fi

			if [ -e $RESULTS_DIR/_test_predictions.csv ]; then
				echo $RESULTS_DIR/_test_predictions.csv already exists. Skipping.
				continue
			fi

			if [ ! -d $TEST_DATA_DIR ] || [ "`find $TEST_DATA_DIR -name *.jpg | wc -l`" -lt "1000" ]; then  
				echo Test data in $TEST_DATA_DIR does not exist. Skipping.
				echo ${DATASET}_${GROUPING}_collection_test >> ${DATASET_ROOT}/missing
				continue
			fi

			echo Results in $RESULTS_DIR

			./wp2 | tee ${RESULTS_DIR}/predict.log
		done
	done
done
