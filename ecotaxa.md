# Usage of the ecotaxa binary

## Environment Variables

The executable is configured via environment variables. Each is documented below

### Required environment

- MODEL_DIR: Contains the weights (_epoch-X.cnn) and the class file (classes.txt)
- OUTPUT_DIR: Is an existing temporary directory, where the results are put. After running SCN, it will contain one or more of {training,validation,testing,unlabeled}_{predictions,confusion,features}.csv
- LD_LIBRARY_PATH: Has to contain the CUDA libraries (e.g. /home/mschroeder/cuda-8.0/lib64)

###  Input data

- TRAINING_DATA_FN, VALIDATION_DATA_FN, TESTING_DATA_FN: Path to an input file, should be labeled.
- UNLABELED_DATA_FN: Path to an input file, labels are not used

### Action toggles

- EPOCH: Epoch to start from. Default: 99 (An epoch is a training step. Typically networks are trained over 100 epochs.)
- STOP_EPOCH:  Default: 0. If EPOCH < STOP_EPOCH, training occurs; otherwise training is skipped
- PREDICT: Default: 0. Whether to predict identifications based on the trained model.
- DUMP_FEATURES: Default 1. Whether to dump the features of the objects in the input file
- PRELOAD_FILES: Load all images in the beginning? Default: 0

### Training Parameters

- LEARNING_RATE = 0.003
- DECAY = 0.05

## Input Data

Input data has the format (objid,path,label) and looks like:

```
32648191,/path/to/detritus/32648191.jpg,detritus
32605994,/path/to/detritus/32605994.jpg,detritus
32603092,/path/to/Neoceratium_furca/32603092.jpg,Neoceratium_furca
32618469,/path/to/multiple/32618469.jpg,multiple
32643507,/path/to/Rhabdonella/32643507.jpg,Rhabdonella
32588245,/path/to/detritus/32588245.jpg,detritus
32532309,/path/to/detritus/32532309.jpg,detritus
32538872,/path/to/Rhizosolenids/32538872.jpg,Rhizosolenids
32681933,/path/to/detritus/32681933.jpg,detritus
32680546,/path/to/detritus/32680546.jpg,detritus
```

##  Output Data

Feature dumps have the format (objid,numeric label,features...) and look like:

```
17,1,-0.00645129,-0.0022401,-0.000265544,0.00881543,...
90,4,-0.00916182,-0.00302664,-0.00114029,0.00858517,...
```

### 