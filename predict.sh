#!/usr/bin/env bash
# Predict using the given results directory

: "${1:?One argument needs to be given}"
: "${DATASET_ROOT:?DATASET_ROOT needs to be nonempty}"

cat $1/wp2.cfg

set -a
source $1/wp2.cfg
set +a

export RESULTS_DIR=$1/
export START_EPOCH=99
export STOP_EPOCH=99

TEST_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_test/

if [ -d "$TEST_DATA_DIR" ] && [ "`find $TEST_DATA_DIR -name *.jpg | wc -l`" -gt "0" ]; then  
    export TEST_DATA_DIR
else
	echo Test data in $TEST_DATA_DIR does not exist.
fi

./wp2
