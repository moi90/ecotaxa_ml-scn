/*
 * OpenCVPictureWithID.h
 *
 *  Created on: 18.09.2017
 *      Author: moi
 */

#ifndef OPENCVPICTUREWITHID_H_
#define OPENCVPICTUREWITHID_H_

#include "OpenCVPicture.h"

class OpenCVPictureWithID: public OpenCVPicture {
public:
	OpenCVPictureWithID(std::string filename, std::string objectId, unsigned char backgroundColor = 128,
            int label_ = -1);
	virtual ~OpenCVPictureWithID();

	std::string objectId;

	std::string identify();
};

#endif /* OPENCVPICTUREWITHID_H_ */
