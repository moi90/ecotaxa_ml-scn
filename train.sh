#!/usr/bin/env bash

: "${DATASET_ROOT:?DATASET_ROOT needs to be nonempty}"
: "${RESULTS_ROOT:?RESULTS_ROOT needs to be nonempty}"


echo Using datasets ${DATASETS:=flowcam uvp5ccelter zoocam zooscan}
echo Using groupings ${GROUPINGS:=group1 group2}
echo Using splits ${SPLITS:=0 1 2}

for DATASET in $DATASETS; do
	export DATASET
	echo Dataset $DATASET
	for GROUPING in $GROUPINGS; do
		export GROUPING
		echo Grouping $GROUPING

		for SPLIT in $SPLITS; do
			export SPLIT
			echo Split $SPLIT

			export TRAINING_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_${SPLIT}_train
			export VALIDATION_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_${SPLIT}_val
			export CLASS_LIST_FN=${DATASET_ROOT}/${DATASET}_classes_${GROUPING}.txt
			export TEST_DATA_DIR=
			export RESULTS_DIR=${RESULTS_ROOT}/SCN_${DATASET}_${GROUPING}_${SPLIT}_`date +%Y-%m-%d-%H-%M`/
			mkdir -p $RESULTS_DIR

			echo Results are written to $RESULTS_DIR

			./wp2 | tee -a ${RESULTS_DIR}/wp2.log
		done
	done
done
