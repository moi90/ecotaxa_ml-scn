#pragma once
#include "SpatiallySparseDataset.h"
#include "OpenCVPicture.h"
#include "ClassList.h"
#include "types.h"

#include <map>

class SpatiallySparseDatasetOpenCVIndex: public SpatiallySparseDataset {
public:
	std::map<std::string, int> classes;
	SpatiallySparseDatasetOpenCVIndex(ClassList &classList, std::string indexFile,
			batchType type_, int backgroundCol = 128, bool loadData = true,
			int flags = 1);
};
