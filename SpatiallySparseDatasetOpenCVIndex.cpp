#include "SpatiallySparseDatasetOpenCVIndex.h"
#include <iostream>
#include <string>
#include <cassert>
#include <thread>
#include <fstream>
#include <iterator>
#include <libgen.h>
#include <fstream>
#include <vector>
#include <thread>
#include "OpenCVPictureWithID.h"

void loadDataThread(std::vector<Picture *> *pictures, int flags, unsigned int k,
		int n);

void inline getNextField(const std::string line, char delim,
		std::string &result, std::string::size_type &pos) {
	std::string::size_type delimPos = line.find(delim, pos);

	if (delimPos == std::string::npos) {
		result = line.substr(pos);
		pos = line.length();
		return;
	}

	result = line.substr(pos, delimPos - pos);
	pos = delimPos + 1;
}

SpatiallySparseDatasetOpenCVIndex::SpatiallySparseDatasetOpenCVIndex(
		ClassList &classList, std::string indexFile, batchType type_,
		int backgroundCol, bool loadData, int flags) {

	// Set properties of SpatiallySparseDataset
	name = indexFile;
	type = type_;
	nClasses = classList.nClasses();

	if (flags == 0)
		nFeatures = 1;
	if (flags == 1)
		nFeatures = 3;

	// Load images from indexFile (object id, path, label)
	std::ifstream f(indexFile.c_str());
	std::string cl;
	int ctr = 0;
	while (f >> cl) {
		std::string objId, imagePath, imageLabel;
		std::string::size_type dpos = 0;

		getNextField(cl, ',', objId, dpos);
		getNextField(cl, ',', imagePath, dpos);
		getNextField(cl, ',', imageLabel, dpos);

		if (objId.empty() || imagePath.empty()) {
			throw std::runtime_error(
					"Error: ObjectID and imagePath must not be empty.");
		}

		/* std::cout << objId << "|" << imagePath << "|" << imageLabel
		 << std::endl; */

		// Lookup label in classes
		int numLabel = classList.getNumeric(imageLabel);

		if (type_ != UNLABELEDBATCH && numLabel == -1) {
			std::stringstream error;
			error << "Error: Unknown label in a labeled dataset: "
					<< imageLabel;

			throw std::runtime_error(error.str());
		}

		pictures.push_back(
				new OpenCVPictureWithID(imagePath, objId, backgroundCol,
						numLabel));
	}

	if (loadData) {
		std::vector<std::thread> workers;
		int nThreads = 4;
		for (int nThread = 0; nThread < nThreads; ++nThread)
			workers.emplace_back(loadDataThread, &pictures, flags, nThread,
					nThreads);
		for (int nThread = 0; nThread < nThreads; ++nThread)
			workers[nThread].join();
	}
}
