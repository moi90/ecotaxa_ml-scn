/*
 * OpenCVPictureWithID.cpp
 *
 *  Created on: 18.09.2017
 *      Author: moi
 */

#include "OpenCVPictureWithID.h"

OpenCVPictureWithID::OpenCVPictureWithID(std::string filename, std::string objectId,
		unsigned char backgroundColor, int label_) :
		OpenCVPicture(filename, backgroundColor, label_) {
	this->objectId = objectId;
}

OpenCVPictureWithID::~OpenCVPictureWithID() {}

std::string OpenCVPictureWithID::identify() {
	return objectId;
}
