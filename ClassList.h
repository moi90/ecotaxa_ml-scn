/*
 * ClassList.h
 *
 *  Created on: 15.09.2017
 *      Author: mschroeder
 */

#pragma once

#include <map>

class ClassList {
public:
	std::map<std::string, int> classes;

	ClassList(std::string classesListFile);

	int nClasses();

	void summary();

	int getNumeric(std::string label);
};

