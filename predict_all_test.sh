#!/usr/bin/env bash
# Predict all results directories containing a .keep file

: "${RESULTS_ROOT:?RESULTS_ROOT needs to be nonempty}"
: "${DATASET_ROOT:?DATASET_ROOT needs to be nonempty}"
: "${CUDA_VISIBLE_DEVICES:?CUDA_VISIBLE_DEVICES needs to be nonempty}"

echo Using datasets ${DATASETS:=flowcam uvp5ccelter zoocam zooscan}
echo Using groupings ${GROUPINGS:=group1 group2}

for DATASET in $DATASETS; do
	export DATASET
	echo Dataset $DATASET
	for GROUPING in $GROUPINGS; do
		export GROUPING
		echo Grouping $GROUPING

		export TRAINING_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_train/
		export VALIDATION_DATA_DIR=
		export CLASS_LIST_FN=${DATASET_ROOT}/${DATASET}_classes_${GROUPING}.txt
		export TEST_DATA_DIR=${DATASET_ROOT}/${DATASET}_${GROUPING}_collection_test
		TRAIN_RESULTS_DIR=${RESULTS_ROOT}/SCN_${DATASET}_${GROUPING}/
		export RESULTS_DIR=${RESULTS_ROOT}/SCN_${DATASET}_${GROUPING}_TEST/
		export START_EPOCH=99
		export STOP_EPOCH=99

		if [ ! -d $TRAIN_RESULTS_DIR ]; then
			echo $TRAIN_RESULTS_DIR is missing. Skipping.
			continue
		fi

		mkdir -p $RESULTS_DIR

		if [ ! -e $RESULTS_DIR/_epoch-99.cnn ]; then
			ln -s "../SCN_${DATASET}_${GROUPING}/_epoch-99.cnn" $RESULTS_DIR/
		fi

		if [ -e $RESULTS_DIR/_test_predictions.csv ]; then
			echo $RESULTS_DIR/_test_predictions.csv already exists. Skipping.
			continue
		fi

		echo Results in $RESULTS_DIR

		./wp2 | tee ${RESULTS_DIR}/predict.log
	done
done
